package com.news.locationappender

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.view.View
import android.view.Window
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.*
import com.google.android.material.snackbar.Snackbar
import com.google.zxing.client.android.Intents
import com.news.locationappender.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity(), CaptureManager.ResultCallback {

    private val REQUEST_CODE_CAMERA_LOCATION = 101
    private lateinit var binding: ActivityMainBinding
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback
    private var alertDialog: AlertDialog? = null
    private lateinit var barcodeScanner: BarcodeScanner
    private var scanResult: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        barcodeScanner =
            BarcodeScanner(binding.zxingBarcodeScanner, this, this, BarcodeScanner.CodeType.QR_CODE)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                stopLocationUpdates()
                showLoading(false)
                val lastLocation = locationResult.lastLocation
                var url = scanResult
                url = String.format(
                    "%s?lat=%f&long=%f",
                    url,
                    lastLocation.latitude,
                    lastLocation.longitude
                )

                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "https://$url"

                AlertDialog.Builder(this@MainActivity)
                    .setTitle("Here is the result")
                    .setMessage(url)
                    .setPositiveButton(
                        "Open"
                    ) { paramDialogInterface, _ ->
                        paramDialogInterface.dismiss()
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                        startActivity(intent)
                    }
                    .setNegativeButton("Cancel") { dialog, _ ->
                        showLoading(false)
                        dialog.dismiss()
                        checkCameraAndLocationPermission()
                    }
                    .setCancelable(false)
                    .show()
            }
        }
    }

    private fun checkCameraAndLocationPermission() {
        val locationGranted =
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        val cameraGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)

        if (locationGranted != PackageManager.PERMISSION_GRANTED || cameraGranted != PackageManager.PERMISSION_GRANTED) {
            requestCameraLocationPermission()
        } else {
            permissionGranted()
        }
    }

    private fun permissionGranted() {
        barcodeScanner.startDecode()
        barcodeScanner.resume()
    }

    private fun permissionDenied() {
        val errorMessage =
            "Need Camera permission to scan the code & Need Location permission to get current location"
        if (errorMessage.isNotEmpty()) {
            Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_LONG).show()
        }
    }

    private fun requestCameraLocationPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA),
            REQUEST_CODE_CAMERA_LOCATION
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            permissionDenied()
        } else {
            permissionGranted()
        }
    }

    private fun startLocationUpdates() {
        if (isLocationEnabled()) {
            showLoading(true)
            val locationRequest = LocationRequest.create()
            locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            locationRequest.interval = 5
            fusedLocationClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.getMainLooper()
            )
        } else {
            requestLocationEnable()
        }
    }

    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    private fun requestLocationEnable() {
        if (!isLocationEnabled()) {
            AlertDialog.Builder(this)
                .setMessage("Enable location service?")
                .setPositiveButton(
                    "Open settings"
                ) { paramDialogInterface, _ ->
                    paramDialogInterface.dismiss()
                    startActivity(
                        Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    )
                }
                .setNegativeButton("Cancel") { dialog, _ ->
                    showLoading(false)
                    dialog.dismiss()
                }
                .setCancelable(false)
                .show()
        }
    }

    private fun isLocationEnabled() = (getSystemService(Context.LOCATION_SERVICE) as LocationManager).isProviderEnabled(LocationManager.GPS_PROVIDER)

    private fun showLoading(loading: Boolean) {
        if (loading) {
            alertDialog = getAlertDialog(this)
            alertDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            alertDialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            alertDialog!!.show()
        } else {
            alertDialog?.dismiss()
        }
    }

    private fun getAlertDialog(
        context: Context
    ): AlertDialog {
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        val customLayout: View =
            layoutInflater.inflate(R.layout.dialog_loading, null)
        builder.setView(customLayout)
        val dialog = builder.create()
        dialog.setCanceledOnTouchOutside(false)
        return dialog
    }

    override fun onScanResult(intent: Intent) {
        val result = intent.getStringExtra(Intents.Scan.RESULT)
        scanResult = result ?: ""
        barcodeScanner.pause()
        startLocationUpdates()
    }

    override fun onResume() {
        super.onResume()
        checkCameraAndLocationPermission()
    }

    override fun onPause() {
        super.onPause()
        barcodeScanner.pause()
    }

    override fun onDestroy() {
        barcodeScanner.destroy()
        super.onDestroy()
    }
}