package com.news.locationappender;

import android.app.Activity;
import android.content.Intent;

import com.google.zxing.integration.android.IntentIntegrator;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

public class BarcodeScanner {

    private DecoratedBarcodeView mDecoratedBarcodeView;
    private CaptureManager mCaptureManager;

    public enum CodeType {QR_CODE, BAR_CODE, ALL}

    /**
     * Get Barcode scanner instance
     *
     * @param decoratedBarcodeView Decorated barcode view
     * @param activity             activity in which scanner is used
     * @param callback             Result callback for getting the scan result
     */
    public BarcodeScanner(DecoratedBarcodeView decoratedBarcodeView, Activity activity
            , CaptureManager.ResultCallback callback, CodeType codeType) {
        mDecoratedBarcodeView = decoratedBarcodeView;
        decoratedBarcodeView.setStatusText(""); //clear scan message text
        mCaptureManager = new CaptureManager(activity, decoratedBarcodeView);
        mCaptureManager.setCallBack(callback);
        IntentIntegrator integrator = new IntentIntegrator(activity);
        switch (codeType) {
            case QR_CODE:
                integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                break;
            case BAR_CODE:
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                break;
        }
        Intent scanIntent = integrator.createScanIntent();
        mCaptureManager.initializeFromIntent(scanIntent, null);
    }

    /**
     * Start to scan and decode the Barcode
     * Also draw the overlay focus rect
     */
    public void startDecode() {
        if (mCaptureManager != null) {
            mCaptureManager.decode();
        }
    }

    /**
     * Resume scanning
     */
    public void resume() {
        if (mCaptureManager != null) {
            mCaptureManager.onResume();
        }
    }

    /**
     * Pause scanning
     */
    public void pause() {
        if (mCaptureManager != null) {
            mCaptureManager.onPause();
        }
    }

    /**
     * Destroy Scanning
     */
    public void destroy() {
        if (mCaptureManager != null) {
            mCaptureManager.onDestroy();
            mCaptureManager = null;
            mDecoratedBarcodeView = null;
        }
    }
}